import { Component, Input, OnInit } from '@angular/core';
import { InputFeature } from 'src/app/models/input.model';
import { FormInputTypeEnum, FormInputVariantEnum } from 'src/app/shared/enums';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
  readOnly= false;
  @Input() inputFeature: InputFeature = {
    variant: FormInputVariantEnum.horizontal,
    label: 'abc',
    disabled: false,
    readonly: false,
    type: FormInputTypeEnum.password,
    formControlName: "input",
    placeholder: "my placeholder"
  };

  constructor() {

  }

  ngOnInit() {

  }

}
