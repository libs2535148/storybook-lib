
import type { Meta, StoryObj } from '@storybook/angular';
import { argsToTemplate } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { InputComponent } from './input.component';
import { FormInputTypeEnum, FormInputVariantEnum } from 'src/app/shared/enums';

export const actionsData = {
  onPinTask: action('onPinTask'),
  onArchiveTask: action('onArchiveTask'),
};

const meta: Meta<InputComponent> = {
  title: 'Input',
  component: InputComponent,
  excludeStories: /.*Data$/,
  tags: ['autodocs'],
  render: (args: InputComponent) => ({
    props: {
      ...args,
      onPinTask: actionsData.onPinTask,
      onArchiveTask: actionsData.onArchiveTask,
    },
    template: `
    <div style="display: flex; width: 100%">
      <app-input ${argsToTemplate(args)}></app-input>
    </div>
  `,
  }),
};

export default meta;
type Story = StoryObj<InputComponent>;

export const Default: Story = {
  args: {
    inputFeature: {
      id: 1,
      placeholder: 'input',
      value: 'value',
      label: 'label',
      variant: FormInputVariantEnum.horizontal,
    },
  },
};

export const Vertical: Story = {
  args: {
    inputFeature: {
      ...Default.args?.inputFeature,
      type: FormInputTypeEnum.password,
      variant: FormInputVariantEnum.horizontal,
    },
  },
};

export const Horizontal: Story = {
  args: {
    inputFeature: {
      ...Default.args?.inputFeature,
      type: FormInputTypeEnum.password,
      variant: FormInputVariantEnum.horizontal,
    },
  },
};
