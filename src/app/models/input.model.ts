import { FormInputTypeEnum, FormInputVariantEnum } from "../shared/enums";

export interface InputFeature {
  id?: number;
  placeholder?: string;
  value?: string;
  label?: string;
  disabled?: boolean;
  readonly?: boolean;
  type?: FormInputTypeEnum;
  variant?: FormInputVariantEnum;
  formControlName?: string;
  required?: boolean;
}
