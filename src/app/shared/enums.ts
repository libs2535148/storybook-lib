export enum FormInputVariantEnum {
  vertical = "vertical",
  horizontal = "horizontal"
}

export enum FormInputTypeEnum {
  password = "password",
  search = "search",
  text = "text"
}
